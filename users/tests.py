from django.contrib.auth.models import User
from django.test import TestCase
from django.test import client
from .forms import *

class Setup_Class(TestCase):

    def setUp(self):
        self.user = User.objects.create(username="a_username", email="example@gmail.com", password1="pass123456", password2="pass123456")

class User_Form_Test(TestCase):

    # Valid Form Data
    def test_UserCreateForm_valid(self):
        form = UserCreateForm(data={'username': "a_username", 'email': "example@gmail.com", 'password1': "pass123456", 'password2': "pass123456"})
        print(form.errors)
        self.assertTrue(form.is_valid())

    # Invalid Form Data
    def test_UserCreateForm_invalid(self):
        form = UserCreateForm(data={'username': "", 'email': "mp", 'password1': "mp", 'password2': ""})
        self.assertFalse(form.is_valid())

class LogInTest(TestCase):
    def setUp(self):
        self.credentials = {
            'username': 'testuser',
            'password': 'secret'}
        User.objects.create_user(**self.credentials)
    def test_login(self):
        # send login data
        response = self.client.post('/users/login/', self.credentials, follow=True)
        # should be logged in now
        self.assertTrue(response.context['user'].is_active)
