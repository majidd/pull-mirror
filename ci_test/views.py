from django.views.generic import TemplateView
from django.urls import reverse
from django.http import HttpResponseRedirect


class HomePage(TemplateView):
    template_name = 'index.html'


class GoodBye(TemplateView):
    template_name = 'goodbye.html'

class Welcome(TemplateView):
    template_name = 'welcome.html'
