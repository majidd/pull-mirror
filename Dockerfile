FROM python:3.6
RUN pip install --default-timeout=100 django  django-bootstrap4 coverage
RUN useradd -ms /bin/bash admin
USER admin
COPY . /ci_test
WORKDIR /ci_test
CMD ['python', 'manage.py']
